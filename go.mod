module golang_nsq

go 1.13

require (
	github.com/bitly/go-nsq v1.0.7
	github.com/golang/snappy v0.0.1 // indirect
)
