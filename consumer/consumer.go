package main

import (
	"github.com/bitly/go-nsq"
	"log"
	"sync"
)

func main() {

	config := nsq.NewConfig()
	q, _ := nsq.NewConsumer("topic", "ch", config)
	var wg sync.WaitGroup
	wg.Add(2)

	go worker(&wg, q)
	wg.Wait()

}

func worker(wg *sync.WaitGroup, q *nsq.Consumer) {
	q.AddHandler(nsq.HandlerFunc(func(message *nsq.Message) error {
		log.Printf("Got a message: %v", (string(message.Body)))

		wg.Done()
		return nil
	}))
	err := q.ConnectToNSQD("127.0.0.1:4150")
	if err != nil {
		log.Panic("Could not connect")
	}

}
